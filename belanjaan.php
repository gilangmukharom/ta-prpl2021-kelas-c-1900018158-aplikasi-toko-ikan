<?php
session_start();
require 'koneksi.php';
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/style2.css" rel="stylesheet" type="text/css" />
    <script src="../js/script.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="fonts/fontawesome-free/css/all.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?familyRoboto" rel="stylesheet">
    <title>Form Pencarian barang</title>
</head>

<body>
    /*navbar*/
    <nav class="navbar navbar-expand-lg navbar-light bg-warning fixed-top">
        <div class="container">
            <h3><i class="fab fa-opencart mr-2"></i></h3>
            <a class="navbar-brand font-weight-bold" href="#">Fish Store</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mr-4">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Beranda <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Reseller <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Hubungi Kami <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Bantuan <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
                <div class="icon mt-2">
                    <h5>
                        <i class="fas fa-cart-plus ml-3 mr-3"></i>
                        <i class="fas fa-envelope mr-3"></i>
                        <i class="fas fa-bell mr-3"></i>
                    </h5>
                </div>
            </div>
        </div>
    </nav>

    <div class="row mt-5 no-gutters">
        <div class="col-md-2 bg-light">
            <ul class="list-group list-group-flush p-2 pt-4">
                <li class="list-group-item bg-warning"><i class="fas fa-list mr-2"></i>Kategori</li>
                <li class="list-group-item"><i class="fas fa-angle-right"></i> <a href="nemo.php" style="text-decoration: none;"> Nemo Fish </a></li>
                <li class="list-group-item"><i class="fas fa-angle-right"></i> <a href="betta.php" style="text-decoration: none;"> Betta Fish </a></li>
                <li class="list-group-item"><i class="fas fa-angle-right"></i> <a href="gold.php" style="text-decoration: none;"> Gold Fish </a></li>
                <li class="list-group-item"><i class="fas fa-angle-right"></i> <a href="arwana.php" style="text-decoration: none;"> Arwana Fish </a></li>
                <li class="list-group-item"><i class="fas fa-angle-right"></i> <a href="belanjaan.php" style="text-decoration: none;"> Pencarian Barang </a></li>
            </ul>
        </div>
    </div>


    <div class="uk-overflow-auto container" style='width: 70%'>
                    <table class="uk-table uk-table-hover uk-table-middle uk-table-divider">
                        <thead>
                            <tr>
                                <th class="uk-table-shrink"></th>
                                <th class="uk-table-small sty">
                                    <h3>ITEM</h3>
                                </th>
                                <th class="uk-table-small"></th>
                                <th class="uk-width-small sty">
                                    <h3>SEARCH</h3>
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                        <form method="POST">
                                <td><input type='text' class='cari' name='cariin'></td>
                                <?php
                                    if($_POST['cariin'] == 'reddragonarwana')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";

                                    }
                                    else if($_POST['cariin'] == 'OrangeGiantArwana')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'Gold Silver Arwana')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'Green Dream Arwana')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'Local White Arwana')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'Yellow Silver Guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'Black Yellow Guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'Red Velvet Guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'Orange Sky Guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'Avatar Yellow')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                    else if($_POST['cariin'] == 'guppy')
                                    {
                                        echo"
                                        <div class='col-md-10'>

                                        <h4 class='text-center font-weight-bold m-4'>Produk Terbaru</h4>
                                        <div class='row'>
                                            <div class='container'>
                                                <div class='row'>
                                                    <div class='container'>
                                                        <div class='row'>
                                                            <div class='col-md-4 col-lg-4'>
                                                                <div class='card' style='width: 18rem;'>
                                                                    <img src='img/guppy/1.jpg' class='card-img-top' alt='...'>
                                                                    <div class='card-body'>
                                                                        <h3 class='card-title'>Yellow Silver Guppy</h3>
                                                                        <h4 class='card-text'>Rp.100.000</h4>
                                                                        <a class='btn btn-primary'>Add to Cart</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }
                                        
                                ?>
                        </form>
                    </table>
                
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="container">
            <div class="wthree_footer_copy">
                <p>© 2021 Fish Store | Design by <a href="http://instagram.com/gilangmukharom">Gilang Mukharom</a>
                </p>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
    -->
</body>

</html>